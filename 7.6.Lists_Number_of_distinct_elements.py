#__autora__ : "Ingrid Guaraca"
#__email__ : "ingrid.guaraca@unl.edu.ec"
# Given a list of numbers with all elements sorted in ascending order, determine and print the number of distinct
# elements in it.

a = [int(i) for i in input().split()]
num = 1
for i in range(1, len(a)):
  if a[i-1] != a[i]:
    num += 1
print(num)