#__autora__ : "Ingrid Guaraca"
#__email__ : "ingrid.guaraca@unl.edu.ec"
# Given two positive integers n and m, create a two-dimensional array of size n×m and populate it with the characters
# "." and "*" in a chequered pattern. The top left corner should have the character "." .

n, m = [int(s) for s in input().split()]
a = [["."]*m for i in range(n)]
for i in range (n):
  for j in range (m):
    if j%2 != 0 and i%2 == 0:
      a[i][j] = '*'
    elif j%2 == 0 and j%2 != 0:
      a[i][j] = '*'
for k in a:
    print(*k)