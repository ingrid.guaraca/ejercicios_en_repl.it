#__autora__ : "Ingrid Guaraca"
#__email__ : "ingrid.guaraca@unl.edu.ec"
# Given a list of integers, find the first maximum element in it. Print its value and its index (counting with 0).

a = [int(s) for s in input().split()]
print(max(a), a.index(max(a)))