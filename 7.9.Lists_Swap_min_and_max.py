#__autora__ : "Ingrid Guaraca"
#__email__ : "ingrid.guaraca@unl.edu.ec"
# Given a list of distinct numbers, swap the minimum and the maximum and print the resulting list.

a = [int(s) for s in input().split()]
max, min = a.index(max(a)), a.index(min(a))
a[max], a[min] = a[min], a[max]
print(a)