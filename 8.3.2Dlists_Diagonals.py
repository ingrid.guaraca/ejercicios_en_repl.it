#__autora__ : "Ingrid Guaraca"
#__email__ : "ingrid.guaraca@unl.edu.ec"
# Given an integer n, create a two-dimensional array of size n×n according to the following rules and print it:
# On the main diagonal put 0.
# On the diagonals adjacent to the main put 1.
# On the next adjacent diagonals put 2, and so forth.

a = int(input())
dim1 = [i for i in range(a)]
dim2 = [[]]*a
for j in range(a):
  dim2[j] = dim1[j:0:-1]
  for l in dim1[:a-j]:
    dim2[j].append(l)
  print(*dim2[j])