#__autora__ : "Ingrid Guaraca"
#__email__ : "ingrid.guaraca@unl.edu.ec"
# Given an odd positive integer n, produce a two-dimensional array of size n×n. Fill each element with the character
# "." . Then fill the middle row, the middle column and the diagonals with the character "*".  You'll get an image of
# a snow flake. Print the snow flake in n×n rows and columns and separate the characters with a single space.

a = int(input())
n = [['.'] * a for i in range(a)]
for i in range(a):
  n[i][i] = "*"
  n[a // 2][i] = "*"
  n[i][a // 2] = "*"
  n[i][a - i - 1] = "*"
for rows in n:
  print(" ".join(rows))