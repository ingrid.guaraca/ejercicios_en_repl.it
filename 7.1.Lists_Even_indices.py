#__autora__ : "Ingrid Guaraca"
#__email__ : "ingrid.guaraca@unl.edu.ec"
# Given a list of numbers, find and print all its elements with even indices (i.e. A[0], A[2], A[4], ...).

a = [int(i) for i in input().split()]
indice= 0
for i in a:
  if indice % 2 == 0:
    print (i)
  indice += 1