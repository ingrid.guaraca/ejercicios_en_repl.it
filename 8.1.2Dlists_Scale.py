#__autora__ : "Ingrid Guaraca"
#__email__ : "ingrid.guaraca@unl.edu.ec"
# Given two integers - the number of rows m and columns n of m×n 2d list - and subsequent m rows of n integers,
# followed by one integer c. Multiply every element by c and print the result.

d= input().split()
NUM_ROWS= int(d[0])
NUM_COLS= int(d[1])
a = [[int(j) for j in input().split()] for i in range(NUM_ROWS)]
b= int(input())
for i in range(NUM_ROWS):
  for j in range(NUM_COLS):
    a[i][j] *= b
for fila in a:
  for columna in fila:
    print(columna, end= " ")
  print()