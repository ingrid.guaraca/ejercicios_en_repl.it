#__autora__ : "Ingrid Guaraca"
#__email__ : "ingrid.guaraca@unl.edu.ec"
# Given two integers - the number of rows m and columns n of m×n 2d list - and subsequent m rows of n integers,
# followed by two non-negative integers i and j less than n, swap the columns i and j of 2d list and print the
# result.

NUM_ROWS, NUM_COLS = [int(s) for s in input().split()]
a = [[int(j) for j in input().split()] for i in range(NUM_ROWS)]
m,n = [int(s) for s in input().split()]
for i in range(NUM_ROWS):
  a[i][m], a[i][n] = a[i][n], a[i][m]
for res in a:
  print(*res)