#__autora__ : "Ingrid Guaraca"
#__email__ : "ingrid.guaraca@unl.edu.ec"
# In bowling, the player starts with 10 pins in a row at the far end of a lane. The goal is to knock all the pins down.
# For this assignment, the number of pins and balls will vary. Given the number of pins N and then the number of balls
# K to be rolled, followed by K pairs of numbers (one for each ball rolled), determine which pins remain standing
# after all the balls have been rolled.

N, K = [int(s) for s in input().split()]
pin = ['I'] * N
for i in range(K):
  left, right = [int(s) for s in input().split()]
  for i in range(left - 1, right):
    pin[i] = '.'
print(''.join(pin))