#__autora__ : "Ingrid Guaraca"
#__email__ : "ingrid.guaraca@unl.edu.ec"
# Given a list of numbers, find and print all its elements that are greater than their left neighbor.

a = [int(s) for s in input().split()]
for s in range(1, len(a)):
  if a[s] > a[s - 1]:
    print(a[s], end = " ")