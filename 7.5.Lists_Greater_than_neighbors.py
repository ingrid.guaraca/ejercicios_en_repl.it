#__autora__ : "Ingrid Guaraca"
#__email__ : "ingrid.guaraca@unl.edu.ec"
# Given a list of numbers, determine and print the number of elements that are greater than both of their neighbors.
# The first and the last items of the list shouldn't be considered because they don't have two neighbors.

a = [int(s) for s in input().split()]
num = 0
for s in range(1, len(a) - 1):
  if a[s - 1] < a[s] > a[s + 1]:
    num += 1
print(num)