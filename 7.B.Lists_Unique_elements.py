#__autora__ : "Ingrid Guaraca"
#__email__ : "ingrid.guaraca@unl.edu.ec"
# Given a list of numbers, find and print the elements that appear in it only once. Such elements should be printed in
# the order in which they occur in the original list.

a = [int(s) for s in input().split()]
for s in range(len(a)):
  for i in range(len(a)):
    if s != i and a[s] == a[i]:
        break
  else:
    print(a[s], end= " ")