#__autora__ : "Ingrid Guaraca"
#__email__ : "ingrid.guaraca@unl.edu.ec"
# Given an integer n, create a two-dimensional array of size n×n according to the following rules and print it:
# On the antidiagonal put 1.
# On the diagonals above it put 0.
# On the diagonals below it put 2.

a = int(input())
n = [[0] * a for i in range(a)]
for i in range(a):
  for j in range(a):
    if i + j + 1 < a:
      n[i][j] = 0
    elif i + j + 1 == a:
      n[i][j] = 1
    else:
      n[i][j] = 2
for resul in n:
  print(*resul)