#__autora__ : "Ingrid Guaraca"
#__email__ : "ingrid.guaraca@unl.edu.ec"
# Given two integers - the number of rows m and columns n of m×n 2d list - and subsequent m rows of n integers, find
# the maximal element and print its row number and column number. If there are many maximal elements in different
# rows, report the one with smaller row number. If there are many maximal elements in the same row, report the one
# with smaller column number.

m,n = [int(i) for i in input().split()]
for i in range(0,m):
    a = list(map(int, input().split()))
    if i == 0:
        maximal = max(a)
        j = [0,a.index(max(a))]
    elif max(a) > maximal:
        maximal = max(a)
        j = [i,a.index(max(a))]
print(*j, sep =' ')