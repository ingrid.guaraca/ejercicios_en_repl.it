#__autora__ : "Ingrid Guaraca"
#__email__ : "ingrid.guaraca@unl.edu.ec"
# It is possible to place 8 queens on an 8×8 chessboard so that no two queens threaten each other. Thus, it requires
# that no two queens share the same row, column, or diagonal.
# Given a placement of 8 queens on the chessboard. If there is a pair of queens that violates this rule, print YES,
# otherwise print NO. The input consists of eight coordinate pairs, one pair per line, with each pair giving the
# position of a queen on a standard chessboard with rows and columns numbered from 1 to 8.

queens=8
n=[]
m=[]
for i in range(queens):
  col_n, fil_m =[int(s) for s in input().split()]
  n.append(col_n)
  m.append(fil_m)
right= True
for i in range (queens):
  for k in range (i+ 1,queens):
    if n[i]== n[k] or m[i]==m[k] or abs (n[i]-n[k])==abs(m[i]- (m[k])):
      right= False
if right:
  print ("NO")
else:
  print ("YES")