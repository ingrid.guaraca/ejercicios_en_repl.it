#__autora__ : "Ingrid Guaraca"
#__email__ : "ingrid.guaraca@unl.edu.ec"
# Given a list of non-zero integers, find and print the first adjacent pair of elements that have the same sign.
# If there is no such pair, print 0.

a = [int(i) for i in input().split()]
i = 1
for i in range(1, len(a)):
  if a[i] * a[i-1] > 0:
    print(str(a[i - 1]), str(a[i]))
    break
  elif i == len(a)-1:
    print("0")