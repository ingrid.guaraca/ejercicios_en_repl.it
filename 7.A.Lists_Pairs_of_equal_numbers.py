#__autora__ : "Ingrid Guaraca"
#__email__ : "ingrid.guaraca@unl.edu.ec"
# Given a list of numbers, count a number of pairs of equal elements. Any two elements that are equal to each other
# should be counted exactly once.

a = [int(s) for s in input().split()]
pares = 0
for s in a:
  for i in range (0,len(a)):
    if a[i]==s and i != a.index(s):
      pares = pares + 1
pares = pares/2
print(int(pares))